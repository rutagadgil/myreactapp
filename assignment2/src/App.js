import React, {Component} from 'react';
import './App.css';

import Validation from './Validation/Validation'
import Char from './Char/Char'

class App extends Component{

  state = {
    text: ''
  };

  inputChangeHandler = (event) => {
    const currText = event.target.value;
    this.setState({text: currText});
  };

  deleteCharHandler = (charIndex) => {
    let currText = this.state.text;
    let currChars = currText.split('');
    currChars.splice(charIndex, 1);
    currText = currChars.join('');
    this.setState({text: currText});
  }

  render(){
    let text = this.state.text;
    const chars = text.split('');
    const charComponents = chars.map((char, charIndex) => {
      return (
        <Char 
        value={char} 
        key={charIndex} 
        clicked={this.deleteCharHandler.bind(this, charIndex)}/>
      )
    });

    return (
      <div className="App">
        <h1>Assignment 2</h1>
        
        <input type="text" value={this.state.text} onInput={this.inputChangeHandler} />
        <p className="text-length">Text Length: {this.state.text.length}</p>
        <Validation textLength={this.state.text.length} />
        <div>{charComponents}</div>
      </div>
    );
  }
}

export default App;
