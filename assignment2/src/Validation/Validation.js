import React from 'react';

const validation = (props) => {
    let validation = (
        <p>Text too short! At least 6 chars required</p>
    );
    if (props.textLength > 5) {
        validation = (<p>Text long enough</p>)
    }
    return validation;
}

export default validation;