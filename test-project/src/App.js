import React, {Component} from 'react';
import './App.css';
import Person from './Person/Person'

class App extends Component {
  state = {
    persons: [
      {id: 1, name: 'Ruta', age: 29},
      {id: 2, name: 'Shweta', age: 53},
      {id: 3, name: 'Rajeev', age: 58},
      {id: 4, name: 'Varad', age: 33}
    ],
    showPersons: false
  };

  togglePersonsHandler = () => {
    const currentValue = this.state.showPersons;
    this.setState({showPersons: !currentValue})
  };

  deletePersonHandler = (personIndx) => {
    alert('deleting person', personIndx)
    const personsClone = [...this.state.persons];
    personsClone.splice(personIndx, 1);
    this.setState({persons: personsClone});
  };

  updatePersonNameHandler = (personIndx, event) => {
    const persons = [...this.state.persons]
    persons[personIndx].name = event.target.value;
    this.setState({persons: persons});
    event.preventDefault();
  };

  render() {

    let persons = null;
    if (this.state.showPersons) {
      <div>
        {
          persons = (
          this.state.persons.map ((person, personIndx) => {
              return (
                <Person 
                click={this.deletePersonHandler.bind(this, personIndx)}
                key={person.id} 
                name={person.name} 
                age={person.age} 
                changed={this.updatePersonNameHandler.bind(this, personIndx)}
                />
              );
            })
          )
        }
        </div>
    }
    return (
      <div className="App">
        <h1>Toggle persons here:</h1>
        <button onClick={this.togglePersonsHandler}>Toggle persons</button>
        {persons}
      </div>
    );
  }
}

export default App;
