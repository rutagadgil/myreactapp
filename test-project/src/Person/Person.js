import './Person.css'

const person = (props) => {
    
    return (
        <div className='Person'>
            <p onClick={props.click}>Name: {props.name}</p>
            <input type="text" defaultValue={props.name} onChange={props.changed} />
            <p>Age: {props.age}</p>
        </div>
    );
}

export default person;