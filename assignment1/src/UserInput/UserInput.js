import React from 'react';

const userInput = (props) => {
    const style = {
        fontFamily: 'cursive'
    }

    return (
        <input style={style} type='text' value={props.userName} onChange={props.clickHandler}/>
    );
};

export default userInput;