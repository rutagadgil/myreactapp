import React, { Component } from 'react';
import './App.css';

import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

class App extends Component{

  state = {
    userName1: 'Ruta',
    userName2: 'Shweta',
    userName3: 'Rajeev'
  };
  
  clickHandler = (event) => {
    // console.log("event handled yay!");
    this.setState({
      userName1: event.target.value
    })
  }
  
  render() {
    return (
      <div className="App">
        <h1>Assignment 1</h1>
        <UserInput userName={this.state.userName1} clickHandler={this.clickHandler.bind(this)}/>
        <UserOutput userName={this.state.userName1} />  
        <UserOutput userName={this.state.userName2} />  
        <UserOutput userName={this.state.userName3} />
      </div>
    );
  }
}

export default App;
